/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql.jdbc.java;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author DOAN DUY DAT
 */
public class Schedule {
    public Schedule(){
        
    }
    public static void timerTask(){
        Timer timer = new Timer();
        TimerTask taskNew;
        taskNew = new TimerTask() {
            @Override
            public void run() {
                Date dateNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("E hh:mm:ss a zzz dd.MM.yyyy" );
                System.out.println(ft.format(dateNow));
            }
        };
        timer.schedule(taskNew, 0, 2000);
    }
}
