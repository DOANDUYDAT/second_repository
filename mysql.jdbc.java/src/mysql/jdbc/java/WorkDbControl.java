/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysql.jdbc.java;

import java.sql.SQLException;

/**
 *
 * @author DOAN DUY DAT
 */

public class WorkDbControl {
    
    public WorkDbControl(){
        
    }
    
    public static void getWork(String table_name) throws SQLException{
        String sql = "SELECT * FROM " + table_name;
        ResultUtils.rs = ResultUtils.state.executeQuery(sql);
    }
    
    public void insertWork(String table_name, String work_name, String 
            time_start) throws SQLException {
        String sql = "INSERT INTO " + table_name + " (work_id, work_name, time_start) VALUES (, " 
                + work_name + ", " + time_start + ");";
        ResultUtils.rs = ResultUtils.state.executeQuery(sql);
    }
    
    public void updateInf(String table_name, int work_id, String new_work_name,
            String new_time_start, String new_time_end) throws SQLException{
        String sql = "UPDATE " + table_name + " SET work_name = " + new_work_name
                + ", time_start = " + new_time_start + " WHERE work_id = " 
                + String.valueOf(work_id) + ";";
        ResultUtils.rs = ResultUtils.state.executeQuery(sql);
    }
    
    public void deleteWork(String table_name, int work_id) throws SQLException{
        String sql = "DELETE FROM " + table_name + "WHERE work_id = "
                + String.valueOf(work_id) + ";";
        ResultUtils.rs = ResultUtils.state.executeQuery(sql);
    }
}
