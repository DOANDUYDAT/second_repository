package mysql.jdbc.java;

import java.awt.AWTException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import lthdt.group7.sub09.view;

public class ConnectionUtils {
 
  public static Connection getMyConnection() throws SQLException,
          ClassNotFoundException {
      // Sử dụng MySQL
      // Bạn có thể thay thế bởi Database nào đó.
      return MySQLConnUtils.getMySQLConnection();
  }
    
    //
    // Test Connection ...
    //
    public static void main(String[] args) throws IOException, AWTException, 
            SQLException, ClassNotFoundException {
        ResultUtils.resultUtils();
        
        view mainWindow = new view();
        mainWindow.setVisible(true);
        mainWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        TrayIcon.trayIcon();
        
        
    }
    
}